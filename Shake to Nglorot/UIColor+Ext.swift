//
//  UIColor+Ext.swift
//  Shake to Nglorot
//
//  Created by Prayudi Satriyo on 22/08/19.
//  Copyright © 2019 Techrity. All rights reserved.
//

import UIKit

extension UIColor {
    
    static func random() -> UIColor{
        let red     = CGFloat.random(in: 0...1)
        let green   = CGFloat.random(in: 0...1)
        let blue    = CGFloat.random(in: 0...1)
        
        return UIColor(red:red, green:green, blue:blue, alpha:1.0)
    }

}
